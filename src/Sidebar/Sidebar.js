import React from 'react';
import {NavLink, Link} from 'react-router-dom';
import './Sidebar.scss';

const Sidebar = ()=>{
    return(
        <div className="sidebar__container">
            <div className="sidebar__user-profile-container">
                <span className="sidebar__user-profile__image">
                    <img src="assets/images/man.svg" alt="" className="sidebar__user-profile--img"/>
                </span>
                <h4 className="sidebar__user-profile--name">Abhishek Mishra</h4>
                <p className="sidebar__user-profile--email">abhishek.mishra@gmail.com</p>
            </div>
            <div className="sidebar__nav-container">
                <div className="sidebar__nav-links">
                    <h3>Browse</h3>
                    <ul>
                        <li>
                            <Link to="/"><img src="assets/images/home.svg"/>Home</Link>
                        </li>
                        <li>
                            <Link to="/playlist"> <img src="assets/images/playlist.svg"/>Playlist</Link>
                        </li>
                        <li>
                            <Link to="/artist"><img src="assets/images/user.svg"/>Artist</Link>
                        </li>
                        <li>
                            <Link to="/album"><img src="assets/images/album.svg"/>Album</Link>
                        </li>
                    </ul>
                </div>
                <div className="sidebar__nav-links">
                    <h3>Discover</h3>
                    <ul>
                        <li>
                            <Link to="/"><img src="assets/images/radio.svg"/>radio</Link>
                        </li>
                        <li>
                            <Link to="/playlist"> <img src="assets/images/event.svg"/>event</Link>
                        </li>
                        <li>
                            <Link to="/artist"><img src="assets/images/mic.svg"/>Podcast</Link>
                        </li>
                        <li>
                            <Link to="/album"><img src="assets/images/heart.svg"/>For you</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Sidebar;