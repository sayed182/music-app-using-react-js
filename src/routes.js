import React from "react";

const routes = [
    {
        path: "/",
        exact: true,
        sidebar: () => <div>home!</div>,
        main: () => <h2>Home</h2>
    },
    {
        path: "/playlist",
        sidebar: () => <div>playlist!</div>,
        main: () => <h2>Playlist</h2>
    },
    {
        path: "/artist",
        sidebar: () => <div>shoelaces!</div>,
        main: () => <h2>artist</h2>
    }
];

export default routes;