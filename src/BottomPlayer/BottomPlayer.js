import React, {useState, useRef, useEffect} from "react";
import './BottomPlayer.scss';

const BottomPlayer = ()=>{
    const [audio] = useState(new Audio("/assets/audio.mp3"));
    const [playing, setPlaying] = useState(false);
    const [audioDuration, setAudioDuration] = useState(null);
    const [timeLeft, setTimeLeft] = useState(()=>{return ("00:00")});
    const [seekbarPosition, setSeekbarPosition] = useState(0);
    useEffect(()=>{
        const interval = setInterval(() => {
           const timeString = calculateRunningTime(audio.duration, audio.currentTime );
           if(timeString){
               setTimeLeft(timeString);
               setSeekbarPosition(parseInt(audio.currentTime));
           }
        }, 1000);
        return () => clearInterval(interval);
   }, []);

    useEffect(()=>{
        audio.onloadedmetadata = function() {
            let time = new Date(audio.duration * 1000).toISOString().substr(14, 5);
            setAudioDuration(time);
        };
      playing ? audio.play()
          :audio.pause();
    },[playing]);

    const handleChange = (event)=>{
        setSeekbarPosition(event.target.value);
        audio.currentTime = event.target.value;
    }
    const handleSeebar = (event)=> {
        console.log(event.clientX);
        console.log(event.target.clientLeft);
    }
    const handleSlide = (event) =>{
        console.log(event.clientX)
    }
    return (
        <div className="bottom__container">
            <div className="music__album">
                <div className="music__album__image">
                    <img src="assets/images/album-thumb.svg" className="music__album__image--img" alt="Album Thumbnail Art"/>
                </div>
                <div className="music__album__description">
                    <p className="music__album__description--name">Dil Tod ke</p>
                    <small className="music__album__description--code">AHDTY4G</small>
                </div>
            </div>
            <div className="music__player">
                <div className="player-controls">
                    <div className="previous-song">
                        <img src="assets/images/backward-solid.svg" alt=""/>
                    </div>
                    <div className="pause-song" onClick={()=>{setPlaying(!playing)}}>
                        <span>| |</span>
                    </div>
                    <div className="next-song">
                        <img src="assets/images/forward-solid.svg" alt=""/>
                    </div>
                </div>
                <div className="player-timeline">
                    <div className="song-time">{audioDuration }</div>
                    <input
                           min="0"
                           max={parseInt(audio.duration)}
                           value={seekbarPosition}
                           type="range"
                           onChange={handleChange}
                    />

                    <div className="song-time">{timeLeft}</div>
                </div>
            </div>
            <div className="music__options">
                <img src="assets/images/heart-solid.svg" alt=""/>
                <img src="assets/images/playlist-player.svg" alt=""/>
                <img src="assets/images/repeat.svg" alt=""/>
            </div>
        </div>
    );
}

function calculateRunningTime(trackTime, currentTime){
        let timeElapsed = trackTime - currentTime;
        let timeString = new Date(timeElapsed * 1000).toISOString().substr(14, 5);
        return timeElapsed >= 0 ? timeString:false;
}



export default BottomPlayer;