const server = require('express')();
const fetch = require('node-fetch');
const port = 8000;


server.get('/', (req, res) => {
    res.send('Hello World!')
})

const AUTHORIZE_URL= "https://accounts.spotify.com/authorize";
const CLIENT_ID = "aaff3dd9885442bb9f4003967042f251";
const RESPONSE_TYPE = "code";
const REDIRECT_URI = "http://localhost:8000/callback";
const SCOPE = "user-read-private user-read-email";


server.get('/login', (req, res)=> {
    res.redirect('https://accounts.spotify.com/authorize' +
        '?response_type=code' +
        '&client_id=' + CLIENT_ID +
        (SCOPE ? '&scope=' + encodeURIComponent(SCOPE) : '') +
        '&redirect_uri=' + encodeURIComponent(REDIRECT_URI));
});

server.get('/callback', (req, res) => {
    const AUTH_CODE =  req.query.code;
    const body = new URLSearchParams(
        {
            'grant_type': 'authorization_code',
            'code': AUTH_CODE,
            'redirect_uri': 'http://localhost:8000/callback',
            'client_id': 'aaff3dd9885442bb9f4003967042f251',
            'client_secret': '247ea61799684534ae6810fc9e9a96ba',
        }
    );
    console.log(body);
    const response = fetch( 'https://accounts.spotify.com/api/token', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: body,
    });
    response.then((value) => value.json())
        .then(json => res.send(json));

})

server.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});