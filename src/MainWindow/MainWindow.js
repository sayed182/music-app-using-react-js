import React from "react";
import './MainWindow.scss';
import {BrowserRouter as Router, Route, Switch, NavLink} from "react-router-dom";
import routes from "../routes";
import NavigationBar from "./NavigationBar";
import TopMusic from "./TopMusic";
import RecommandedAlbum from "./RecommandedAlbum";
import PopularMusic from "./PopularMusic";

const MainWindow = ({featured, releases, categores})=>{
    return(
        <main>
        <NavigationBar/>
        <TopMusic/>
        <div style={{display: "flex", marginTop: "40px"}}>
            <PopularMusic/>
            <RecommandedAlbum/>
        </div>
        </main>
    );
}
export default MainWindow;