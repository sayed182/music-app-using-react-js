import React from "react";
const NavigationBar = ()=>{
    return (
        <div className="navigation-bar">
            <div className="search-bar">
                <span><img src="/assets/images/search.svg" alt=""/></span>
                <input type="text" placeholder="Search for Song, artist, etc...."/>
            </div>
            <div>
                <button id="upgrade-button">Upgrade to premnium</button>
                <span id="setting"><img src="/assets/images/settings.svg" alt=""/></span>
                <span id="notification"><img src="/assets/images/notifications-none.svg" alt=""/></span>
            </div>
        </div>
    );
}

export default NavigationBar;