import React from "react";

const RecommandedAlbum = ()=>{
    return (
        <div className="recommanded-album__container">
            <h1>Recommanded Album</h1>
            
            <div className="recommanded-album__art">
                <div>
                    <img src="/assets/albumArts/2.jpg" alt="album art" className="album-img"/>
                    <img src="/assets/images/vinly.svg" alt="album art" className="album-vinly"/>
                </div>
            </div>
        </div>
    );
}

export default RecommandedAlbum;