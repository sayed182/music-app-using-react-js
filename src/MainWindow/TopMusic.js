import React from "react";
import './TopMusic.scss';

const TopMusic = ()=>{
    return(
        <div className="top-music__container">
            <h1>Top Music</h1>
            <div className="album__container">
                <div><img src="/assets/albumArts/1.jpg" alt="top albums"/></div>
                <h3>FEFE</h3>
                <small className="disabled">6ix9, Niki Minaj</small>
            </div>
        </div>   
    )
}

export default TopMusic;