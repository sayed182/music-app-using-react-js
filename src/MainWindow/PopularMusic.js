import React from "react";

const PopularMusic = () =>{
    return(
        <div className="popular-music__container">
            <h1>Popular Music</h1>
            <ul className="popular-music__list">
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
                <li>
                    <span className="track-play"><img src="assets/images/play.svg" alt=""/></span>
                    <div className="music-thumb"><img src="/assets/albumArts/4.jpg" alt=""/></div>
                    <div className="track__detail">
                        <h4 className="track__detail--name">Available</h4>
                        <small className="track__detail--artist">Justine Biber</small>
                    </div>
                    <div className="track-time">4:68</div>
                    <div className="track-like"><img src="/assets/images/heart.svg" alt=""/></div>
                </li>
            </ul>
        </div>
    )
}

export default PopularMusic;